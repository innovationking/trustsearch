<?php
return [
    'search_engine' => array(
        '1' => 'TrustPilot',
        '2' => 'TrustedShops',
    ),
    'search_engine_url' => array(
        '1' => 'https://www.trustpilot.com/review/'
    )
];
