<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //dd('sdgsdg');
    $search_engine_list = config('setting.search_engine');
    $search_text = '';
    $search_engine = array();
    return view('welcome',compact('search_engine_list','search_text', 'search_engine'));
});
Route::post('/', 'ScrapController@index');
