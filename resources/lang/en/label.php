<?php
return [
    /*
     * Front End
     */
    'search_text_required_error_msg'    => "Please specify domain name you want to search.",
    'search_text_string_error_msg'      => "Invalid search string, please try again.",
    'search_text_regex_error_msg'       => "Invalid domain name, please try again.",
    'general_error_message_one'         => "Oops!",
    'general_error_message_two'         => "There were problems with the entry:",
    'search_engine_required_error_msg'  => "Please select at least one option to search.",
    'search_engine_array_error_msg'     => "Invalid search engine option format.",
    'search_engine_distinct_error_msg'  => "Search engine option must be unique.",
    'search_engine_integer_error_msg'   => "Invalid search engine option, please try again.",
    'search_engine_min_error_msg'       => "Minimum one search engine option require to search.",
];
