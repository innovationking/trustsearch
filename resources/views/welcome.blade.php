<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>My first search engine</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        .m-b-md-footer {
            margin-bottom: 10px;
            font-weight: bold;
        }

        .checked{
            color: orange;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        <div class="title m-b-md">
            My first search engine
        </div>
        <form action="" name="my_search_engine" method="post">
        <input type="hidden" name="_token" value="{{csrf_token()}}">
        <div class="row">
            @if (count($errors) > 0)
                <div class="col-md-8 offset-1 p-3">
                    <div class="alert alert-danger">
                        <div class="row flex-center">
                            @foreach ($errors->all() as $error)
                                <span>{{ $error }}</span>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif
        </div>
        <div class="row">
            <div class="col-md-8 offset-1 p-3">
                <div class="form-group">
                    {{ Form::text('search_text', old('search_text',$search_text), array_merge(['class' => 'form-control', 'id' => 'search_text', 'placeholder' => 'Enter any domain name'])) }}
                </div>
            </div>
            <div class="col-md-1 p-3">
                {{ Form::button('Search', ['type' => 'submit', 'class' => 'btn btn-warning btn-sm shadow']) }}
            </div>
        </div>
        <div class="row">
            @if(isset($search_engine_list) && !empty($search_engine_list))
                @foreach($search_engine_list as $keyEngine => $valueEngine)
                    <div class="col-md-3 offset-1 p-3">
                        @php
                            $checked = false;
                        @endphp
                        @if(in_array($keyEngine,$search_engine))
                            @php
                                $checked = true;
                            @endphp
                        @endif
                        {{Form::checkbox('search_engine[]', $keyEngine, old($valueEngine, $checked))}}&nbsp;&nbsp;{{$valueEngine}}
                    </div>
                @endforeach
            @endif
        </div>
        </form>

        @if(isset($search_result))
            <hr>
            @if(empty($search_result))
                <div class="row">
                    No records found.
                </div>
            @else
                <div class="row p-5">
                    Found {{count($search_result)}} Results <br>
                </div>
                <div class="row">
                    <div class="col-lg-12 p-5 bg-white rounded shadow-sm mb-5">

                        <!-- Shopping cart table -->
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                @foreach($search_result as $keySearch => $valueSearch)
                                    <tr>
                                        <th scope="row" class="border-0">
                                            <div class="text-left">
                                                @if(!empty($valueSearch['image']))
                                                    <img src="{{$valueSearch['image']}}" alt="" width="70" class="img-fluid rounded shadow-sm">
                                                @else
                                                    <img src="https://cdn.trustpilot.net/brand-assets/1.1.0/logo-white.svg" alt="" width="70" class="img-fluid rounded shadow-sm">
                                                @endif
                                                <div class="ml-3 d-inline-block align-middle">
                                                    <h5 class="mb-0"> <span class="text-dark d-inline-block align-middle">{{ucfirst($valueSearch['search_text'])}}</span></h5>
                                                    @php $is_claimed = 'No'; @endphp
                                                    @foreach($valueSearch['is_claimed'] as $keyData => $valueData)
                                                        @if($keyData == "pages/cpp/company-status2/claimed/label" && $valueData == "Claimed")
                                                            @php $is_claimed = 'Yes'; @endphp
                                                        @endif
                                                    @endforeach
                                                    <span class="text-muted font-weight-normal font-italic d-block">Reviews - {{$valueSearch['reviewCount']}}. Claimed - {{$is_claimed}}. Country - {{$valueSearch['addressCountry']}}</span>
                                                    <span class="text-muted font-weight-normal font-italic d-block">
                                                    @for($i=1;$i<=5;$i++)
                                                        @if(round($valueSearch['ratingValue']) >= $i)
                                                            <span class="fa fa-star checked"></span>
                                                        @else
                                                            <span class="fa fa-star"></span>
                                                        @endif
                                                    @endfor

                                                    {{$valueSearch['ratingValue']}} / {{$valueSearch['bestRating']}}

                                                    @if(!empty($valueSearch['categories']))
                                                            Category - {{implode(', ',$valueSearch['categories'])}}
                                                    @endif
                                                    </span>
                                                </div>
                                            </div>
                                        </th>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- End -->
                    </div>
                </div>
            @endif
        @endif
    </div>
</div>
<hr>
<footer class="footer">
    <div class="container">
        <span class="text-muted flex-center m-b-md-footer">My Search engine 2019</span>
    </div>
</footer>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</body>
</html>
