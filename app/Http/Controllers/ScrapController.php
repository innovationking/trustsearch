<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\ScrapValidationRequest;
use Illuminate\Support\Facades\Redirect;
use Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;

class ScrapController extends Controller
{
    public function index(ScrapValidationRequest $request)
    {
        $userRequest = $request->all();
        $search_engine_list = config('setting.search_engine');
        $is_exist = false;
        foreach($userRequest['search_engine'] as $keyEngine => $valueEngine){
            if(array_key_exists($valueEngine,$search_engine_list)){
                $is_exist = true;
            }
        }
        if($is_exist){
            $search_result_list = array();
            foreach($userRequest['search_engine'] as $keyEngine => $valueEngine){
                $search_result_list[$keyEngine] = $this->get_scrap_data($valueEngine,$userRequest['search_text']);
            }
            $search_engine_list = config('setting.search_engine');

            return view('welcome', array(
                'search_result' => $search_result_list,
                'search_engine_list' => $search_engine_list,
                'search_text' => $userRequest['search_text'],
                'search_engine' => $userRequest['search_engine'],
            ));
        }
        else{
            return Redirect::back()->withErrors([trans('label.search_engine_integer_error_msg')]);
        }
    }

    public function get_scrap_data($search_engine_type = null, $search_text = null){
        if(!empty($search_engine_type) && !empty($search_text)){
            $search_engine_url = config('setting.search_engine_url');
            foreach($search_engine_url as $keySearch => $valueUrl){
                if($keySearch == 1){
                    if(isset(config('setting.search_engine_url')[$search_engine_type]) && !empty(config('setting.search_engine_url')[$search_engine_type])){
                        $crawlerClient = new Client();
                        $crawler = $crawlerClient->request('GET', config('setting.search_engine_url')[$search_engine_type] . $search_text);
                        $google_scheme = (string) $crawler->filter('script[type="application/ld+json"]')->text();
                        $country_name = (string) $crawler->filter('country-selector')->attr('initial-selected-country-name');
                        $data_list = $crawler->filter('script[type="application/json"]')->each(function ($node,$i) {
                            $data = trim($node->text());
                            $category_data = @json_decode($data);
                            return $category_data;
                        });

                        if(!empty($google_scheme)){
                            $generated_string_json = preg_replace('/(;(?!.*;))/', '', $google_scheme, 1);
                            $final_output = @json_decode($generated_string_json);
                            if(isset($final_output[0]) && !empty($final_output[0])){
                                $web_data['search_text'] = $search_text . ' in ' . config('setting.search_engine')[$search_engine_type];
                                $web_data['reviewCount'] = $final_output[0]->aggregateRating->reviewCount;
                                $web_data['addressCountry'] = $country_name;
                                $web_data['ratingValue'] = $final_output[0]->aggregateRating->ratingValue;
                                $web_data['bestRating'] = $final_output[0]->aggregateRating->bestRating;
                                $web_data['categories'] = $data_list[3]->categories;
                                $web_data['is_claimed'] = $data_list[1]->root;
                                $web_data['image'] = $final_output[0]->image;

                                return $web_data;
                            }
                            else{
                                return array();
                            }
                        }
                        else{
                            return array();
                        }
                    }
                    else{
                        return array();
                    }
                }
                else{
                    return array();
                }
            }
        }
        else{
            return array();
        }
    }
}
