<?php
namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
class ScrapValidationRequest extends FormRequest
{
    public function __construct(Request $request)
    {
        /*
         * Initialize variables
         */
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'search_text'       => 'required|string',
            'search_engine'     => 'required|array|min:1',
            'search_engine.*'   => 'required|distinct|integer|min:1',
        ];
    }
    /**
     * Get the validation message that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'search_text.required'      => trans('label.search_text_required_error_msg'),
            'search_text.string'        => trans('label.search_text_string_error_msg'),
            'search_text.regex'         => trans('label.search_text_regex_error_msg'),
            'search_engine.required'    => trans('label.search_engine_required_error_msg'),
            'search_engine.array'       => trans('label.search_engine_array_error_msg'),
            'search_engine.min'         => trans('label.search_engine_min_error_msg'),
            'search_engine.*.required'  => trans('label.search_engine_required_error_msg'),
            'search_engine.*.distinct'  => trans('label.search_engine_distinct_error_msg'),
            'search_engine.*.integer'   => trans('label.search_engine_integer_error_msg'),
            'search_engine.*.min'       => trans('label.search_engine_min_error_msg'),
        ];
    }
}
